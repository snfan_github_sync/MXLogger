## 1.0.9
* Add system share. Delete sqlite3_flutter_libs.
## 1.0.8
* Update Flutter dependencies, set Flutter >=3.3.0 and Dart to >=2.18.0 <4.0.0
## 1.0.7
* Adaptation flutter 3.10.0.
## 1.0.6
* Add hidden future when on double tap.
## 1.0.5
* Add switch 'tag','name','msg'
## 1.0.4
* Remove isDebug arg.
## 1.0.3
* Fix bug.
## 1.0.2
* Use SelectionArea.
## 1.0.1+1
* Add sqlite3_flutter_libs dependencie.
## 1.0.1
* Add isDebugMode arg.
## 1.0.0
* MXLogger debugger.

